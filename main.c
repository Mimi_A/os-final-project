#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <sys/types.h>
#include <signal.h>

#define BUFF_SIZE 256
#define SERVER_PORT 1068
#define CACHE_SIZE 8
#define MAX_CONNECTIONS 10

char sendLine[BUFF_SIZE];//sending to client
char receiveLine[BUFF_SIZE];//recieving from client
int serverSocket;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
typedef struct ListItem ListItem;

struct Storage { // this is the cache
    char data[BUFF_SIZE]; // this is the data for the variable
    char name[BUFF_SIZE]; //this is the name in the variable
};
struct ListItem {
    char name[BUFF_SIZE];
    char value[BUFF_SIZE];
    ListItem *nextItem;
};
struct ListItem *rootNode;
struct Storage cache[CACHE_SIZE];// initialize the cache

void setFirstNode(char name[], char value[]){//pass in NULL if no value //Parameters: &name, &value
    if(rootNode->name[0] == '\0') {
        strcpy(rootNode->name, name);
        strcpy(rootNode->value, value);
    }
    else {
        ListItem *newListItem = (ListItem *)malloc(sizeof(ListItem));
        strcpy(newListItem->name, name);
        strcpy(newListItem->value, value);

        newListItem->nextItem = rootNode;
        rootNode = newListItem;
    }
}
void deleteNode(ListItem * listItem) {
    if(listItem->name[0] != '\0') {
        ListItem *currentNode = rootNode;
        if (strcmp(listItem->name, rootNode->name) == 0) { //Delete root node
            if (listItem->nextItem != NULL) { //Root points to another node
                rootNode = listItem->nextItem;
                free(listItem);
            }
            else { //No other nodes in list; don't free the root
                rootNode = rootNode->nextItem;
            }
        }
        else {
            while (strcmp(currentNode->nextItem->name, listItem->name) != 0) {
                if(currentNode->nextItem->name[0] != '\0') {
                    currentNode = currentNode->nextItem;
                }
                else {
                    break;
                }
            }

            currentNode->nextItem = currentNode->nextItem->nextItem;
            free(listItem);
        }
    }
}
struct ListItem * searchForNode(char name[]) {
    struct ListItem *currentNode = rootNode;

    if(name[0] != '0') {
        while (currentNode->name[0] != '\0') {
            if(strcmp(currentNode->name, name) == 0) { //Node found
                return currentNode;
            }
            else if(currentNode->nextItem->name[0] != '\0') { //Node not found, more to search through
                currentNode = currentNode->nextItem;
            }
            else { //Node not in LL
                return currentNode->nextItem;
            }
        }
    }
}

//Return value of variable
char * returnValue(char name[]) {
    ListItem * node = searchForNode(name);
    return node->value;
}

void sendToLL(char *data, char *name) { // this will call the insert into LL
    setFirstNode(name, data);
}
int hashFunction(char input[]) {// this takes a char aka the data and returns a int
    return (int) strlen(input) % CACHE_SIZE;
}

void readVariable(void * inputName, int * connection){
    char name[BUFF_SIZE];
    strcpy(name, (char *) inputName);
    if (strcmp(cache[hashFunction(name)].name, name) == 0) {//in cache
        snprintf(sendLine, sizeof(sendLine), "%s", cache[hashFunction(name)].data);
        write(*connection, sendLine, strlen(sendLine));
        //strcpy(sendLine, cache[hashFunction(name)].data);
    } else if (searchForNode(name)->name[0]!='\0'){//in LL
        int index = hashFunction(name);

        struct Storage tempStorage;
        strcpy(tempStorage.data, searchForNode(name)->value);
        strcpy(tempStorage.name,searchForNode(name)->name);

        sendToLL(cache[index].data,cache[index].name);//send the variable from the cache to LL first
        cache[index] = tempStorage;//sends to cache

        snprintf(sendLine, sizeof(sendLine), "%s", searchForNode(name)->value);
        write(*connection, sendLine, strlen(sendLine));
    }else{
        snprintf(sendLine, sizeof(sendLine), "%s", "\0");
        write(*connection, sendLine, strlen(sendLine));
    }


}
void deleteVariable(char * nameP) {
    int index = hashFunction(nameP);
    if (strcmp(cache[index].name,nameP)!= 0) { // this happens if the name is not in the linked list
        deleteNode(searchForNode(nameP));
    }
    if (strcmp(cache[index].name,nameP) == 0) { //this looks for the variable name in the cache
        cache[index].data[0] = '\0';// sets the old data to null
        cache[index].name[0] = '\0';// sets the old name to null
    }
}
void updateVariable(char * nameP,char * newData){// TODO fix this
    int index = hashFunction(nameP);
    if (strcmp(cache[index].name,nameP)== 0){ // this sees if the passed in name is in the cache
        strcpy(cache[index].data, newData);
    }
    if(strcmp(cache[index].name,nameP)!= 0){ // this happens if the name is not in the cache
        printf("we need to search the LL for the name, find it, delete it from the LL, and return data here");
        if (searchForNode(nameP)->name[0] == '\0'){// checks to see if the variable is in the LL
            return;
        }else {
            sendToLL(cache[index].data,cache[index].name);//send the variable from the cache to LL first
            strcpy(cache[index].data, newData);//replaces old variable data
            strcpy(cache[index].name, nameP);//replaces the old variable name
            deleteNode(searchForNode(nameP));//get and add data from LL
        }
    }
}
void createVariable(char * nameP, char * dataP){//The input is after 6 characters//TODO take in the actual input
    int index = hashFunction(nameP);
    if (cache[index].name[0] !=  '\0') {// checks to see if that cache has been used
        sendToLL(cache[index].data, cache[index].name);// if there is data already in the cache and will send the data and the name to the LL
        cache[index].data[0] = '\0';// sets the old data to the LL
        cache[index].name[0] = '\0';// sets the old name to the LL
    }
    if (cache[index].name[0] ==  '\0'){// if the cache is empty then it will add the variable
        strcpy(cache[index].data, dataP); // sets the data
        strcpy(cache[index].name, nameP); // sets the name
    }
}

void closeConnection() {
    printf("\nClosing Connection with file descriptor: %d \n", serverSocket);
    close(serverSocket);
    exit(1);
}
void * proccessClientRequest(void * request) {
    pthread_mutex_lock(&mutex);
    int connectionToClient = *(int *) request;



    int bytesReadFromClient =0;
    //read request from client
    while((bytesReadFromClient = read(connectionToClient, receiveLine, BUFF_SIZE))>0) {        receiveLine[bytesReadFromClient] = 0;

        //at this point we can use the data from the client
        printf("Received: %s\n", receiveLine);


        // Returns first token
        char *token = strtok(receiveLine, " ");

        if (strcmp(token, "create") == 0) {

            char *varToCreate = strtok(NULL, " ");
            char *dataToCreate = strtok(NULL, " ");
            if (dataToCreate != NULL) {//there is data
                createVariable(varToCreate, dataToCreate);
            } else {//There is not data
                createVariable(varToCreate, "\0");
            }

        } else if (strcmp(token, "read") == 0) {
            char *varToRead = strtok(NULL, " ");

            readVariable(varToRead,&connectionToClient);
        } else if (strcmp(token, "update") == 0) {
            char *varToRead = strtok(NULL, " ");
            char *newValue = strtok(NULL, " ");

            updateVariable(varToRead, newValue);

        } else if (strcmp(token, "delete") == 0) {
            char *varToDel = strtok(NULL, " ");
            deleteVariable(varToDel);
        } else {
            //invalid command
            snprintf(sendLine, sizeof(sendLine), "Please enter a valid command");
            write(connectionToClient, sendLine, strlen(sendLine));
        }
        //writes to client
        //TODO use snprintf for output to client. example in readVariable
        //snprintf(sendLine, sizeof(sendLine), "Hello client\n");
        //write(connectionToClient, sendLine, strlen(sendLine));
        //clears recieve and closes connection



        bzero(&receiveLine, sizeof(receiveLine));
        close(connectionToClient);
        pthread_mutex_unlock(&mutex);
    }

}

int main() {

    for (int i = 0; i < CACHE_SIZE;i++){
        cache[i].name[0]= '\0';
    }

    //Linked list
    ListItem *root = (ListItem *)malloc(sizeof(ListItem));
    ListItem *nullNode = (ListItem *)malloc(sizeof(ListItem));
    root->name[0] = '\0';
    root->value[0] = '\0';
    nullNode->name[0] = '\0';
    nullNode->value[0] = '\0';
    root->nextItem = nullNode;
    rootNode = root;

    //Malik Testing
    char str[]= "Malik";
    char info[]= "computerscience";
    char newStr[] = "Bryce";
    char newInfo[] = "Bio";
    char updateInfo[] = "Software engineer";

    createVariable(str,info);
    createVariable(newStr,newInfo);
    updateVariable(str,updateInfo);

    int connectionToClient,bytesReadFromClient;

    //creates server socket
    serverSocket = socket(AF_INET, SOCK_STREAM, 0);
    struct sockaddr_in serverAddress;
    bzero(&serverAddress, sizeof(serverAddress));
    serverAddress.sin_family = AF_INET;

    //listens to any address
    serverAddress.sin_addr.s_addr = htonl(INADDR_ANY);
    serverAddress.sin_port = htons(SERVER_PORT);

    //binds port
    if (bind(serverSocket, (struct sockaddr *) &serverAddress, sizeof(serverAddress)) == -1) {
        printf("Unable to bind to port, connection has possibly timed out");
        exit(-1);
    }


    //registers CTRL C to close connection
    struct sigaction sigIntHandler;
    sigIntHandler.sa_handler = closeConnection;
    sigIntHandler.sa_flags = 0;
    sigaction(SIGINT, &sigIntHandler, NULL);

    //listen for connections, max is 10
    listen(serverSocket, MAX_CONNECTIONS);

    while (1) {
        //Accept connection, 2nd parameter specify connection, 3rd parameter is socket length
        connectionToClient = accept(serverSocket, (struct sockaddr *) NULL, NULL);
        pthread_t thread;
        pthread_create(&thread, NULL, proccessClientRequest, (void *) &connectionToClient);

    }

//    Mimi testing linked list
//    char input1[32];
//    char input2[32];
//    char input3[32];
//    char input4[32];
//    strcpy(input1, "Hello");
//    strcpy(input2, "World");
//    strcpy(input3, "Hola");
//    strcpy(input4, "Mundo");
//
//    setFirstNode(&input1, &input2);
//
//    if(*rootNode->name != NULL) {
//        struct ListItem currentItem = *rootNode;
//        printf("%s -> \n", currentItem.name);
//    }
//
//    setFirstNode(&input2, &input1);
//
//    if(*rootNode->name != NULL) {
//        struct ListItem currentItem = *rootNode;
//        printf("%s -> ", currentItem.name);
//        printf("%s -> \n", currentItem.nextItem->name);
//    }
//
//    setFirstNode(&input3, &input4);
//
//    if(*rootNode->name != NULL) {
//        struct ListItem currentItem = *rootNode;
//        printf("%s -> ", currentItem.name);
//        printf("%s -> ", currentItem.nextItem->name);
//        printf("%s -> \n", currentItem.nextItem->nextItem->name);
//    }
//
//    setFirstNode(&input4, &input3);
//
//    if(*rootNode->name != NULL) {
//        struct ListItem currentItem = *rootNode;
//        printf("%s -> ", currentItem.name);
//        printf("%s -> ", currentItem.nextItem->name);
//        printf("%s -> ", currentItem.nextItem->nextItem->name);
//        printf("%s -> \n", currentItem.nextItem->nextItem->nextItem->name);
//    }
//
//    char testInput[32];
//    strcpy(testInput, "Hehe");
//    ListItem testNode;
//    strcpy(testNode.value, returnValue(input4));
//    char data[BUFF_SIZE];
//    strcpy(data, searchForNode(input2)->value);
//    printf("Value for %s: %s  \n\n", input1, data);
//
//    testNode = *searchForNode(input1);
//    deleteNode(&testNode);
//
//    if(*rootNode->name != NULL) {
//        struct ListItem currentItem = *rootNode;
//        printf("%s -> ", currentItem.name);
//        printf("%s -> ", currentItem.nextItem->name);
//        printf("%s -> ", currentItem.nextItem->nextItem->name);
//        printf("%s -> \n", currentItem.nextItem->nextItem->nextItem->name);
//    }
//    while(1){
//        printf("\nhelloasdf\n");
//    }
}
